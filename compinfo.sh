#! /bin/bash 

function System {
  echo -e .........."\033[34mOPER.SYSTEM\033[0m".......
LINUX="`uname --all | grep "Linux"`"
  echo -e `echo "\033[33m$LINUX\033[0m"| cut -d " " -f1,3,4,14,15`                          
}

function Proc {
  echo -e ..........."\033[34mPROC.INFO\033[0m"........
NUMBER_PROC=`cat /proc/cpuinfo | grep  -c "proc"`
  echo -e "\033[32mProcessor       : $NUMBER_PROC\033[0m" 
MODEL_NAME=`cat /proc/cpuinfo | grep  "model name" | head -1`
  echo -e "\033[32m$MODEL_NAME\033[0m"
VENDOR_ID=`cat /proc/cpuinfo | grep "vendor_id" | head -1`
  echo -e "\033[32m$VENDOR_ID\033[0m"
FPU=`cat /proc/cpuinfo | grep "fpu" | head -1`
  echo -e "\033[32m$FPU\033[0m"
}

function HDD {
  echo -e ............."\033[34mH.DISK\033[0m".........
  echo "`lsblk -rn | grep  "sd" | cut -d" " -f1,4`"
}

function RAM {
  echo -e .............."\033[34mRAM\033[0m"...........
Mem="` free -h | grep "Mem"`"
  echo -e "\033[31mTotal=`  echo $Mem | cut -d " " -f2` \033[0m" 
  echo -e "\033[31mUsed=`echo $Mem | cut -d " " -f3` \033[0m"
  echo -e "\033[31mFree=`echo $Mem | cut -d " " -f4` \033[0m"
}

function Internet {
 echo -e ..........."\033[35mETH.CONFIG\033[0m".........
Eth=`ifconfig | grep "eth"`
if [ -n "$Eth" ]
 then 
  echo  -e "\033[36meth : YES\033[0m"
else 
  echo -e "\033[36meth : NO\033[0m"
fi 
Wlan=`ifconfig | grep "wlan"`
if [ "$Wlan" != "" ]
 then 
  echo -e "\033[36mwlan : YES\033[0m"
else 
  echo -e "\033[36mwlan : NO\033[0m"
fi 
}

function Display {
  echo -e ............"\033[34mDISPLAY\033[0m".........
  echo "`xrandr  | grep  "Screen" `"
  echo "`xrandr  | grep  "primary" `"
  echo "`xwininfo -root  | grep  "Visual" | head -1`"
  echo "`xwininfo -root  | grep  "Depth" `"
  echo "`xwininfo -root  | grep  "Color" `"
  echo "`xwininfo -root  | grep  "Corner" `"
  echo "`xwininfo -root  | grep  "State" `"
}

function Help {
  echo "Arguments you have input are incorrect. If you want to input correct arguments you should follow these instructions:
Processor  : --cpuinfo
Hard disks : --hdd
RAM        : --ram
Operating system : --ssm
Ethernet   : --eth
Display    : --display
If you want to get information about all of them you should input  : --all"
}

if [ -z "$*" ]
 then
Help
fi
 for A in {$1,$2,$3,$4,$5,$6}
do
if [ $1 = "--all" ]
 then
System
Display
Proc
RAM
HDD
Internet
elif [ $A = "--cpuinfo" ]
 then
Proc
elif [ $A = "--hdd" ]
 then
HDD
elif [ $A = "--ram" ]
 then
RAM
elif [ $A = "--ssm" ]
 then
System
elif [ $A = "--eth" ]
 then
Internet
elif [ $A = "--display" ]
 then
Display
else
reset
Help
fi
done




